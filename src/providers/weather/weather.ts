import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from "rxjs/Rx";
import { map } from 'rxjs/operators';

/*
  Generated class for the WeatherProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class WeatherProvider {
	api_key = 'af128fbb1646690f';
	weather_data: Observable<any>;
	url;

	constructor(public http: HttpClient) {
		console.log('Hello WeatherProvider Provider');
		this.url = 'http://api.wunderground.com/api/' + this.api_key + '/conditions/q';
	}


	getWeather(city, state){
		// return type of http.get is Observable<Response>
		this.weather_data = this.http.get(this.url + '/' + state + '/' + city + '.json');			
		return this.weather_data;
	}
}
