import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { WeatherProvider } from '../../providers/weather/weather';
import { Storage } from '@ionic/storage';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
	weather:any;
	location:{
		city:string,
		state:string
	}

	constructor(
		public navCtrl: NavController,
		private weather_provider:WeatherProvider,
		private storage:Storage) {

	}

	ionViewWillEnter(){
		this.storage.get('location').then((val) => {
			if(val != null){
				// anything stored in storage is a string, so we need it in JSON
				this.location = JSON.parse(val);
			}else{
				this.location = {
					city: 'Las Vegas',
					state: 'NV'
				}
			}

			this.weather_provider.getWeather(this.location.city,
			this.location.state).subscribe(weather => {
					this.weather = weather.current_observation;
				});

		});
	}



}
